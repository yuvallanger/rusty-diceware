## v1.2.3

Change main public Git repository from [Gitlab](https://gitlab.com/yuvallanger/rusty-diceware) to [Codeberg](https://codeberg.org/kakafarm/rusty-diceware).

## v1.2.2

* Minor Clippy type code improvements (`map(...).collect()` to `fold(...)`).
* Add README.md file.

## v1.2.1

Derive Clone on Worldlist enum. (Thanks @jcgruenhage!)

## v1.1.1

Moving tests into tests directory. Sorry for the spam.

## v1.1.0

Add tests.

## v1.0.0

No change, just bumping due to a fairly stable looking API.

## v0.5.6

Jan Christian Grünhage (@jcgruenhage) surrounded the wordlists in enums and added optional serde feature. Thank you!
