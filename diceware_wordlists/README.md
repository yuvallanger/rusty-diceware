# rusty-diceware wordlists - wordlists for passphrase generation.

A Rustlang library packaging a bunch of wordlists you can easily use in any [Rustlang][rustlang] package.

Originally made for the [Rusty Diceware][rusty-diceware] commandline command.

Please use [Codeberg][codeberg-mirror] for bug reporting, feature requests, or patch submissions - any other Git repository is just a mirror.

[CHANGELOG here](/CHANGELOG.md).

Inspired by the great passphrase generating solution [Diceware][diceware] ([Wayback Machine mirror][diceware-wayback]) invented by [Arnold G. Reinhold][arnold] ([Wayback Machine mirror][arnold-wayback]) and by Randall Monroe’s [xkcd#936][xkcd-936]:

![“Hidden” alt text jokes are a pain in the ass.](/bin/imgs.xkcd.com/comics/password_strength.png)

## Featuring:

* The three wordlists mentioned in EFF's [Diceware Guide][eff-diceware-guide]:
    * [EFF Long Wordlist][eff-long-wordlist].
    * [EFF Short Wordlist #1][eff-short-wordlist-1].
    * [EFF Short Wordlist #2][eff-short-wordlist-2-0].
* The original [Reinhold wordlist][reinhold-wordlist-asc] ([Wayback Machine mirror][reinhold-wordlist-asc-wayback]).
* The [Beale wordlist][beale-wordlist-asc] ([Wayback Machine mirror][beale-wordlist-asc-wayback]).
* The [MiniLock][minilock] ([github][minilock-github])wordlist. (found in the [phrase.js][minilock-phrase-js] file)

## TODO:

* Which one should it be the default behaviour and which one should be decided in a compile flag?
    1. Compile the wordlists into the executable itself (current behaviour).
    2. Load each wordlist during runtime as needed (unimplemented behaviour).

## Mirrors:

* [Codeberg][codeberg-mirror] is the main repository.
* [Gitlab][gitlab-mirror] is just a mirror.
* [Github][github-mirror] is just a mirror.
* [Kakafarm][kakafarm-mirror] is just a mirror.

## Supprt my extremely important and 110% not trivial work 💰:

You can support me at <https://buymeacoffee.com/kakafarm/> and/or tip on <https://liberapay.com/yuvallanger/donate>:

<script src="https://liberapay.com/yuvallanger/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/yuvallanger/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>

[rusty-diceware]: https://crates.io/crates/diceware/

[codeberg-mirror]: <https://codeberg.org/kakafarm/rusty-diceware/>
[kakafarm-mirror]: <https://kaka.farm/~stagit/rusty-diceware/log.html>
[gitlab-mirror]: <https://gitlab.com/yuvallanger/rusty-diceware/>
[github-mirror]: <https://github.com/yuvallanger/rusty-diceware/>

[rusty-diceware]: <https://crates.io/crates/diceware_wordlists>

[eff-diceware-guide]: <https://www.eff.org/dice>
[eff-long-wordlist]: <https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt>
[eff-short-wordlist-1]: <https://www.eff.org/files/2016/09/08/eff_short_wordlist_1.txt>
[eff-short-wordlist-2-0]: <https://www.eff.org/files/2016/09/08/eff_short_wordlist_2_0.txt>

[arnold]: <https://theworld.com/~reinhold/>
[diceware]: <https://theworld.com/~reinhold/diceware.html>

[arnold-wayback]: <https://web.archive.org/web/20220608141106/https://theworld.com/~reinhold/>
[diceware-wayback]: <https://web.archive.org/web/20220817092807/https://theworld.com/~reinhold/diceware.html>

[beale-wordlist-asc]: <https://theworld.com/~reinhold/beale.wordlist.asc>
[reinhold-wordlist-asc]: <https://theworld.com/~reinhold/diceware.wordlist.asc>
[minilock-phrase-js]: <https://github.com/kaepora/miniLock/blob/71dcf431886068c9ec7f563c3e4158153229b202/src/js/lib/phrase.js>

[beale-wordlist-asc-wayback]: <https://web.archive.org/web/20220602072646/https://theworld.com/~reinhold/beale.wordlist.asc>
[reinhold-wordlist-asc-wayback]: <https://web.archive.org/web/20220820102521/https://theworld.com/~reinhold/diceware.wordlist.asc>

[rustlang]: <http://rust-lang.org>

[xkcd-936]: <https://www.explainxkcd.com/wiki/index.php/936>

[minilock]: <http://minilock.io>
[minilock-github]: <https://github.com/kaepora/miniLock/>
